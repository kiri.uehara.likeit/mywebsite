<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TODO App | ログイン</title>
	<!-- BootstrapのCSS読み込み -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- FontAwesomeの読み込み-->
	<script src="https://kit.fontawesome.com/f4ccc568fd.js" crossorigin="anonymous"></script>
</head>

<body class="bg-light">
	<!-- フォームが真ん中にレイアウトされるように、CSSに記載してある -->
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 mb-5 mt-5">
				<h1 class="text-center">
					<i class="fa-solid fa-t"></i>
					<i class="fa-solid fa-o"></i>
					<i class="fa-solid fa-d"></i>
					<i class="fa-solid fa-o"></i>

					<i class="fa-solid fa-a"></i>
					<i class="fa-solid fa-p"></i>
					<i class="fa-solid fa-p"></i>
					/
					<i class="fa-solid fa-l"></i>
					<i class="fa-solid fa-o"></i>
					<i class="fa-solid fa-g"></i>
					<i class="fa-solid fa-i"></i>
					<i class="fa-solid fa-n"></i>
				</h1>
			</div>
		</div>

		<div class="d-flex justify-content-center align-self-center">
			<div class="card">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<!--エラーメッセージ  end  -->

				<div class="card-body">
					<form action="LoginServlet" method="post" style="width: 600px;">
						<div class="form-group row">
							<!-- メールアドレス -->
							<label for="inputLoginId" class="col-3 col-form-label">メールアドレス</label>
							<div class="col-9">
								<input type="text" name="email" id="inputLoginId" class="form-control" value="${email}"
									autofocus>
							</div>
						</div>

						<div class="form-group row">
							<!-- パスワード -->
							<label for="inputPassword" class="col-3 col-form-label">パスワード</label>
							<div class="col-9">
								<input type="password" name="password" id="inputPassword" class="form-control"
									autofocus>
							</div>
						</div>

						<button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
