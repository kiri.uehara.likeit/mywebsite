package model;

import java.io.Serializable;

public class TodoBeans implements Serializable {
  private int id;
  private String title;
  private int userId;

  // ログインセッションを保存
  public TodoBeans(int id) {
    this.id = id;
  }

  // 全てのデータをセット
  public TodoBeans(int id, String title, int userId) {
    this.id = id;
    this.title = title;
    this.userId = userId;
  }

  public TodoBeans() {
    // TODO 自動生成されたコンストラクター・スタブ
  }

  public TodoBeans(String title1) {
    // TODO 自動生成されたコンストラクター・スタブ
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public int getUserId() {
      return userId;
    }

    public void setUserId(int userId) {
      this.userId = userId;
    }
}
