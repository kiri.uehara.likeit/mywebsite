package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.TodoBeans;

public class TodoDao {

  // todo一覧表示
  public List<TodoBeans> findAll(int id) {
    Connection conn = null;
    PreparedStatement st = null;
    List<TodoBeans> todoList = new ArrayList<TodoBeans>();
    try {
      conn = DBManager.getConnection();

      String sql = ("SELECT * FROM todo WHERE user_id = ?;");

      st = conn.prepareStatement(sql);
      st.setInt(1, id);
      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        TodoBeans todo = new TodoBeans();

        todo.setId(rs.getInt("id"));
        todo.setTitle(rs.getString("title"));
        todo.setUserId(rs.getInt("user_id"));

        todoList.add(todo);

      }
      return todoList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {



      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // todo一覧の検索
  public List<TodoBeans> search(String title, int userid) {
    Connection conn = null;
    List<TodoBeans> todoList = new ArrayList<TodoBeans>();
    try {
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM todo WHERE user_id = ? ";

      if (!title.equals("")) {
        sql += (" AND title LIKE ? ");
      }
      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setInt(1, userid);
      if (!title.equals("")) {
        stmt.setString(2, "%" + title + "%");
      }
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        int id1 = rs.getInt("id");
        String title1 = rs.getString("title");
        int userId1 = rs.getInt("user_id");
        TodoBeans todo = new TodoBeans(id1, title1, userId1);

        todoList.add(todo);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoList;
  }



  // titleの重複

  public TodoBeans findTitle(String title) {
    Connection conn = null;
    TodoBeans todoData = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文
      String sql = "SELECT * FROM todo WHERE title = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみに追加
      String title1 = rs.getString("title");
      todoData = new TodoBeans(title1);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoData;

  }

  // 更新
  public void updateTodo(String title, String id) {
    Connection conn = null;
    try {
      // データベース
      conn = DBManager.getConnection();
      String sql = "UPDATE todo SET title = ? WHERE id = ? ";
      // INSERT実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setString(2, id);

      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public TodoBeans todoDetail(String todoId) {
    Connection conn = null;
    try {
      conn = DBManager.getConnection();
      String sql = ("SELECT * FROM todo WHERE id = ?;");

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, todoId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int todoId1 = rs.getInt("id");
      String title = rs.getString("title");
      int userId = rs.getInt("user_id");
      return new TodoBeans(todoId1, title, userId);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // 新規登録
  public void create(String title, int userId) {
    Connection conn = null;
    try {
      conn = DBManager.getConnection();
      String sql = ("INSERT INTO todo (title,user_id) VALUES (?,?)");

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setInt(2, userId);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void Delete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = " DELETE FROM todo WHERE id = ?;";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      // SQLを検索しない時↓
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}

