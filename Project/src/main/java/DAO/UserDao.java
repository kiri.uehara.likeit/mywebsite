package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.UserBeans;


public class UserDao {

  public UserBeans findByLoginInfo(String email, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // SELECT文
      String sql = "SELECT * FROM user WHERE email = ? and password = ?";
  
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, email);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();  
      
      if (!rs.next()) {
        return null;
      }
      int id = rs.getInt("id");
      String name = rs.getString("name");
      return new UserBeans(id,name);
      
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
}
