package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.TodoDao;
import model.TodoBeans;
import model.UserBeans;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      String todoId = request.getParameter("id");

      HttpSession session = request.getSession();
      UserBeans user = (UserBeans) session.getAttribute("userInfo");

      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      TodoDao todoDao = new TodoDao();
      TodoBeans todo = todoDao.todoDetail(todoId);

      request.setAttribute("title", todo.getTitle());
      request.setAttribute("todo", todo);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
      dispatcher.forward(request, response);
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      String id = request.getParameter("id");
      String title = request.getParameter("title");
      String userId = request.getParameter("user_id");

      if (title.equals("")) {
        request.setAttribute("title", title);
        request.setAttribute("errMsg", "入力された内容は正しくありません。");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
        dispatcher.forward(request, response);
        return;
      }
      TodoDao todoDao = new TodoDao();
      todoDao.updateTodo(title, id);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("ListServlet");

	}

}
