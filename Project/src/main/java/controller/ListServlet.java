package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.TodoDao;
import model.TodoBeans;
import model.UserBeans;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ListServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログイン画面にリダイレクト
    HttpSession session = request.getSession();
    UserBeans user = (UserBeans) session.getAttribute("userInfo");

    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    TodoDao dao = new TodoDao();
    List<TodoBeans> list = dao.findAll(user.getId());
    request.setAttribute("user", user);
    request.setAttribute("todoList", list);
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    request.setCharacterEncoding("UTF-8");
    String title = request.getParameter("title");

    HttpSession session = request.getSession();
    UserBeans user = (UserBeans) session.getAttribute("userInfo");

    TodoDao todoDao = new TodoDao();
    List<TodoBeans> todoList = todoDao.search(title, user.getId());

    request.setAttribute("todoList", todoList);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

}
