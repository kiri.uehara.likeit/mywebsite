package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.TodoDao;
import model.TodoBeans;
import model.UserBeans;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      HttpSession session = request.getSession();
      UserBeans user = (UserBeans) session.getAttribute("userInfo");



      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserBeans user = (UserBeans) session.getAttribute("userInfo");
      request.setAttribute("user", user);

      String title = request.getParameter("title");
      request.setAttribute("title", title);
      request.setAttribute("errMsg", "");


      TodoDao todoDao = new TodoDao();
      List<TodoBeans> todoList = todoDao.search(title, user.getId());
      request.setAttribute("todoList", todoList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
      dispatcher.forward(request, response);



	}

}
