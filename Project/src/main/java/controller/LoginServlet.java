package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.UserDao;
import model.UserBeans;
import utill.PasswordEncorder;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
      dispatcher.forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      
      String email = request.getParameter("email");
      String password = request.getParameter("password");
      UserDao userDao =new UserDao();

      String encodestr = PasswordEncorder.encordPassword(password);

      
      UserBeans user = userDao.findByLoginInfo(email, encodestr);
      /** テーブルに該当のデータが見つからなかった場合 * */
      if (user == null) {   
        // リクエストスコープにエラーメッセージ
        request.setAttribute("errMsg", "ログインに失敗しました。");
        // 入力したログインIDを画面に表示
        request.setAttribute("email", email);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        dispatcher.forward(request, response);
        return;
	}
    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user);



      // 一覧のサーブレットにリダイレクト
      response.sendRedirect("ListServlet");
    }
  }
